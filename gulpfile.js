const gulp = require('gulp');
const theo = require('gulp-theo');
const cleanDir = require('gulp-clean-dir');

/* Formats required for design tokens */
const webFormats = [
    'less',
    'scss',
    'map.scss',
    'styl',
    'custom-properties.css',
    'common.js',
    'module.js',
    'cssmodules.css',
    'json',
    'raw.json',
];

gulp.task('generate-design-tokens', () =>
    webFormats.map((format) =>
        gulp.src('./design-tokens/**/design-tokens.json')
            .pipe(theo({
                transform: {type: 'raw'},
                format: {type: format}
            })
        ).pipe(cleanDir('./dist'))
        .pipe(gulp.dest('./dist/'))
    ),
)


gulp.task('generate-styleguide', () =>
    gulp.src('./design-tokens/**/design-tokens.json')
        .pipe(theo({
        transform: {type: 'raw'},
            format: {
                type: 'html',
                options: {
                    transformPropName: name => name.toLowerCase()
            }
        }
    })).pipe(cleanDir('./docs'))
    .pipe(gulp.dest('./docs/'))
)


gulp.task('default', ['generate-design-tokens', 'generate-styleguide']);